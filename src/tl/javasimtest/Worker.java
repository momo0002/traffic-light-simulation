package tl.javasimtest;

import java.io.IOException;

import org.javasim.RestartException;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;
import org.javasim.streams.RandomStream;
import org.javasim.streams.UniformStream;

public class Worker extends SimulationProcess {
	
	public boolean isBusy = false;
	
	int streamseed = 5;
	RandomStream rands  = new UniformStream(0, 40, streamseed);
	
	public void run() {
        while (!terminated())
        {
            isBusy = true;

            while (Controller.NoOfIssues > 0) {
				try {
	            	Controller.NoOfIssues--;
	            	double service_time = rands.getNumber();
	            	
					System.out.printf("Worker started handling issue, time = %.2f" +
							", service_time = %.2f, remaining issues = %d\n",
							time(), service_time, Controller.NoOfIssues);
	            	
					hold(service_time);
					
					System.out.printf("One worker finished issue, time = %.2f\n", time());						
					
				} catch (ArithmeticException | IOException | SimulationException | RestartException e) {
					e.printStackTrace();
				}
            }

            isBusy = false;
            try {
				cancel();
			} catch (RestartException e) {
				e.printStackTrace();
			}
        }
	}
}
