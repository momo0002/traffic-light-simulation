package tl.javasimtest;

public class EventMain {
	
	public static void main(String[] args) {
		// Give control over to Controller class
		Controller c = new Controller();
		c.await();
		System.exit(0);
	}
}
