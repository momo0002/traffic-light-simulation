package tl.javasimtest;

import org.javasim.RestartException;
import org.javasim.Scheduler;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;
//import org.javasim.stats.SimpleHistogram;

public class Controller extends SimulationProcess {
	
	public static int NoOfIssues = 0; // Number of issues/tickets delivered by customers
	public static Worker[] Workers = new Worker[2];
	
	public Controller() {
		Workers[0] = new Worker();
		Workers[1] = new Worker();
	}
	
	class CustomerGenerator extends SimulationProcess {
		public void run() {
			try {
				while (! terminated()) {
					NoOfIssues++;
					System.out.println("Customer phoned in new issue, time = " + currentTime());
					
					// must check if we should awaken a worker
					if (! Workers[0].isBusy) {
						Workers[0].activate();
					} else if (! Workers[1].isBusy) {
						Workers[1].activate();
					}
					hold(10); // wait 10 min until next run
				}
			} catch (SimulationException | RestartException e) {
				e.printStackTrace();
			}
		}
	}

	public void run() {
		try {
			CustomerGenerator c = new CustomerGenerator();
			c.activate();
			
			Scheduler.startSimulation();
			System.out.println("Starting simulation. Time = " + currentTime());
			
			hold(1000);  // Minutes to simulate
			
			// Alternative, collect statistics using sampling
			//SimpleHistogram hist = new SimpleHistogram(0, 20, 10);
			//for (int i = 0 ; i < 1000 ; i += 5) {
			//	hist.setValue(NoOfIssues);
			//	hold(5);
			//}
			//hist.print();
			
			System.out.println("Final issue queue size = " + NoOfIssues);
			System.out.println("Finished simulation. Time = " + currentTime());
			
			Scheduler.stopSimulation();
			c.terminate();
			Workers[0].terminate();
			Workers[1].terminate();
			terminate();
			SimulationProcess.mainResume();
		} catch (SimulationException | RestartException e) {
			e.printStackTrace();
		}
	}
	
	public void await ()
	{
		resumeProcess();
	    SimulationProcess.mainSuspend();
	}
}
