package tl.simtest;

import java.util.Random;

public class TimeStepWorker {

	Random mRand = new Random();

	class Worker {
		public boolean isBusy = false;
		int finish_time = 0;
		
		void startNewIssue(int time) {
			isBusy = true;
			finish_time = time + mRand.nextInt(10) + 1;
		}
		
		boolean checkDoFinish(int time) {
			if (finish_time == time) {
				isBusy = false;
				return true;
			}
			return false;
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TimeStepWorker m = new TimeStepWorker();
		
		int max_workers = 2;
		Worker[] workers = new Worker[max_workers];
		workers[0] = m.new Worker();
		workers[1] = m.new Worker();
		
		int no_of_issues = 0; // number of customer issues in queue
		int next_customer = 0; // first customer issue at time 0
		
		// time is counting minutes
		for (int time = 0 ; time < 100 ; time++) {
			if (time == next_customer) {
				// Customer phones every 10 min
				next_customer = time + 10;
				
				no_of_issues++;
				System.out.println("Customer phoned in new issue, time = " + time);
			}
			
			for (int w = 0 ; w < max_workers ; w++) {
				if (workers[w].isBusy) {
					if (workers[w].checkDoFinish(time)) {
						System.out.println("One worker finished issue, time = " + time);						
					}
				} else {
					// worker free, check if issues in queue
					if (no_of_issues > 0) {
						workers[w].startNewIssue(time);
						no_of_issues--;
						System.out.println("One worker started handling issue, time = " + time +
								", remaining issues = " + no_of_issues);
					}
				}
			}
		}
		System.out.println("Final issue queue size = " + no_of_issues);
	}

}
