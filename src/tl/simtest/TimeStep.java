package tl.simtest;

import java.util.Random;

public class TimeStep {

	public static void main(String[] args) {

		Random rand = new Random();
		
		int max_workers = 2;
		boolean[] isBusy = {false, false};
		int[] finish_time = {0, 0};

		int no_of_issues = 0; // number of customer issues in queue
		int next_customer = 0; // first customer issue at time 0
		
		// time is counting minutes
		for (int time = 0 ; time < 100 ; time++) {
			if (time == next_customer) {
				// Customer phones every 10 min and creates issue
				next_customer = time + 10;
				
				no_of_issues++;
				System.out.println("Customer phoned in new issue, time = " + time);
			}
			
			// go through all workers
			for (int w = 0 ; w < max_workers ; w++) {
				if (isBusy[w]) {
					if (finish_time[w] == time) {
						isBusy[w] = false;
						System.out.println("One worker finished issue, time = " + time);						
					}
				} else {
					// worker free, check if issues in queue
					if (no_of_issues > 0) {
						isBusy[w] = true;
						int issue_length = rand.nextInt(40) + 1;
						finish_time[w] = time + issue_length;
						
						no_of_issues--;
						System.out.println("Worker " + w +
								" started handling issue, time = " + time +
								", length = " + issue_length +
								", remaining issues = " + no_of_issues);
					}
				}
			}
		}
		System.out.println("Final issue queue size = " + no_of_issues);
	}
}
