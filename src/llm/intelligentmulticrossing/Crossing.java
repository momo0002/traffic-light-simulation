package llm.intelligentmulticrossing;

import org.javasim.RestartException;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;

public class Crossing extends SimulationProcess {
	private RoadSynchronizer primaryRoad;
	private RoadSynchronizer secondaryRoad;
	private int lightTime;
	private int numberOfExtensions = 0;
	
	public Crossing(RoadSynchronizer primaryRoad, RoadSynchronizer secondaryRoad, int lightTime) {
		this.setName("Crossing");
		this.primaryRoad = primaryRoad;
		this.secondaryRoad = secondaryRoad;
		this.lightTime = lightTime;
	}
	
	private void toggleLights() throws SimulationException, RestartException {
		if (this.primaryRoad.isRed()) {
			this.primaryRoad.toggleLight();
			this.secondaryRoad.toggleLight();
			
			this.hold(this.lightTime);
		} else {
			//If there is still a car in queue, and green time has not been extended for too long,
			//and the secondary road's queue is below a certain threshold,
			//then keep the light green for a while longer
			if (
				this.primaryRoad.getQueueLength() > 0 &&
				this.numberOfExtensions < 6 &&
				this.secondaryRoad.getQueueLength() < 20
			) {
				this.numberOfExtensions++;
				this.hold(this.lightTime / 3);
			} else {
				this.primaryRoad.toggleLight();
				this.secondaryRoad.toggleLight();
				
				this.numberOfExtensions = 0;
				
				this.hold(this.lightTime);
			}
		}
	}
	
	public int getThroughput() {
		return (this.primaryRoad.getThroughput() + this.secondaryRoad.getThroughput());
	}
	
	public RoadSynchronizer getPrimaryRoadSynchronizer() {
		return this.primaryRoad;
	}
	
	public RoadSynchronizer getSecondaryRoadSynchronizer() {
		return this.secondaryRoad;
	}
	
	public void run() {
		//Initiate road synchronizers
		this.primaryRoad.init(false);
		this.secondaryRoad.init(true);
		
		while (!this.terminated()) {
			try {
				this.toggleLights();
			} catch (SimulationException e) {
				e.printStackTrace();
			} catch (RestartException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String toString() {
		return this.primaryRoad.toString() + this.secondaryRoad.toString();
	}
}