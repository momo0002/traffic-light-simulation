package llm.intelligentmulticrossing;

import java.util.NoSuchElementException;

import org.javasim.RestartException;
import org.javasim.Scheduler;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;

public class Controller extends SimulationProcess {	
	public Controller() {
	}
	
	public void run() {
		int carsPerHour = 700;
		int numberOfRuns = 100;
		
		double primaryRoadMaxLength = 0;
		double secondaryRoadMaxLength = 0;
		
		Road road1, road2, road3, road4, road5, road6, road7, road8;
		RoadSynchronizer roadSynchronizer1, roadSynchronizer2, roadSynchronizer3, roadSynchronizer4;
		Crossing crossing1, crossing2;
		
		this.setName("Controller");
		for (int i = 1; i <= numberOfRuns; i++) {
			System.out.println("Run " + i);
			road1 = new Road("Road 1", carsPerHour);
			road2 = new Road("Road 2", carsPerHour);
			/*road3 = new Road("Road 3", carsPerHour);
			road4 = new Road("Road 4", carsPerHour);*/
			road3 = new Road("Road 3", (int)(carsPerHour / 1.5));
			road4 = new Road("Road 4", (int)(carsPerHour / 1.5));
			road5 = new Road("Road 5", 0);
			road6 = new Road("Road 6", carsPerHour);
			road7 = new Road("Road 7", (int)(carsPerHour / 1.5));
			road8 = new Road("Road 8", (int)(carsPerHour / 1.5));
			
			road1.setStraight(road5);
			road4.setRight(road5);
			
			roadSynchronizer1 = new RoadSynchronizer(new Road[] {road1, road2});
			roadSynchronizer2 = new RoadSynchronizer(new Road[] {road3, road4});
			
			roadSynchronizer3 = new RoadSynchronizer(new Road[] {road5, road6});
			roadSynchronizer4 = new RoadSynchronizer(new Road[] {road7, road8});
			
			crossing1 = new Crossing(roadSynchronizer1, roadSynchronizer2, 30000);
			crossing2 = new Crossing(roadSynchronizer3, roadSynchronizer4, 30000);
			
			try {
				crossing1.activate();
				crossing2.activate();
				Scheduler.startSimulation();
				//System.out.println("Next event: " + this.nextEv());
				//Time to run simulation, in ms
				hold(3600000);
				
				//Empty queue
				try {
					while (true) {
						SimulationProcess nextEv = this.nextEv();
						nextEv.terminate();
					}
				} catch (NoSuchElementException ex) {
					//No more elements in queue					
				}
				
				System.out.printf(
					"Primary road max queue: %f\nSecondary road max queue: %f\n",
					crossing1.getPrimaryRoadSynchronizer().getAveragedLongestQueueLength(),
					crossing1.getPrimaryRoadSynchronizer().getAveragedLongestQueueLength()
				);
				
				primaryRoadMaxLength += crossing1.getPrimaryRoadSynchronizer().getAveragedLongestQueueLength();
				secondaryRoadMaxLength += crossing1.getSecondaryRoadSynchronizer().getAveragedLongestQueueLength();
			} catch (SimulationException e) {
				e.printStackTrace();
			} catch (RestartException e) {
				e.printStackTrace();
			}
			
			SimulationProcess.Current = this;
		}
		
		System.out.println("\n\nPrimary road: " + (primaryRoadMaxLength / numberOfRuns));
		System.out.println("Secondary road: " + (secondaryRoadMaxLength / numberOfRuns));
		
		try {
			Scheduler.stopSimulation();
			SimulationProcess.mainResume();
		} catch (SimulationException e) {
			e.printStackTrace();
		}
	}
	
	public void await () {
		resumeProcess();
	    SimulationProcess.mainSuspend();
	}
}
