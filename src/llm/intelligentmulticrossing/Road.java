package llm.intelligentmulticrossing;

import java.util.Random;

import org.javasim.RestartException;
import org.javasim.SimulationException;

public class Road {
	private int carsInQueue = 0;
	private int longestQueue = 0;
	private int throughput = 0;
	private String name;
	private Road straight;
	private Road right;
	private Random random;
	public CarGenerator carGenerator;
	public CarRemover carRemover;
	
	public Road(String name, int averageCarsPerHour) {
		this.name = name;
		this.random = new Random();
		
		if (averageCarsPerHour > 0) {
			this.carGenerator = new CarGenerator(this, averageCarsPerHour);
		}
	}
	
	public void addCar() throws SimulationException, RestartException {
		this.carsInQueue++;
		if (this.carsInQueue > this.longestQueue) {
			this.longestQueue = this.carsInQueue;
		}
		if (this.carRemover.passivated()) {			
			this.carRemover.activate();
		}
	}
	
	public int getThroughput() {
		return this.throughput;
	}
	
	public int getLongestQueue() {
		return this.longestQueue;
	}
	
	public int getQueueLength() {
		return this.carsInQueue;
	}
	
	public void incrementThroughput() {
		this.throughput++;
	}
	
	public void removeCar() throws SimulationException, RestartException {
		//Decide if car should turn right or continue straight (no left turns allowed unfortunately)
		int randomInt = this.random.nextInt(4);
		
		if (randomInt < 2) {
			//Continue straight
			if (this.straight != null) {
				this.straight.addCar();
			}
		} else {
			//Turn right
			if (this.right != null) {
				this.right.addCar();
			}
		}
		
		this.carsInQueue--;
	}
	
	public void setRight(Road road) {
		this.right = road;
	}
	
	public void setStraight(Road road) {
		this.straight = road;
	}
	
	public String toString() {
		return String.format("%s:\nMax queue length: %d\nCurrent queue length: %d\nThroughput: %d\n\n", this.name, this.longestQueue, this.carsInQueue, this.throughput);
	}
}
