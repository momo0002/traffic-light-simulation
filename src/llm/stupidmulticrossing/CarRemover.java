package llm.stupidmulticrossing;

import java.util.NoSuchElementException;

import org.javasim.RestartException;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;

public class CarRemover extends SimulationProcess {
	private Road road;
	private RoadSynchronizer roadSynchronizer;
	
	public CarRemover(Road road, RoadSynchronizer roadSynchronizer) {
		this.setName("CarRemover");
		this.road = road;
		this.roadSynchronizer = roadSynchronizer;
	}
	
	public void run() {
		while (!this.terminated()) {
			while (!this.roadSynchronizer.isRed() && this.road.getQueueLength() > 0) {
				try {
					this.hold(3000);
					this.road.incrementThroughput();
					this.road.removeCar();
				} catch (SimulationException e) {
					e.printStackTrace();
				} catch (RestartException e) {
					e.printStackTrace();
				}
			}
			
			try {
				this.passivate();
			} catch (RestartException e) {
				e.printStackTrace();
			} catch (NoSuchElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
