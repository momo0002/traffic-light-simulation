package llm.stupidmulticrossing;

import org.javasim.RestartException;
import org.javasim.SimulationException;

public class RoadSynchronizer {
	private Road[] roads;
	private boolean red;
	
	public RoadSynchronizer(Road[] roads) {
		this.roads = roads;
	}
	
	public int getThroughput() {
		int sum = 0;
		
		for (int i = 0; i < this.roads.length; i++) {
			sum += this.roads[i].getThroughput();
		}
		
		return sum;
	}
	
	private void goGreen() throws SimulationException, RestartException {
		this.red = false;
		for (int i = 0; i < this.roads.length; i++) {
			this.roads[i].carRemover.activate();
		}
	}
	
	private void goRed() {
		this.red = true;
	}
	
	public double getAveragedLongestQueueLength() {
		int sum = 0;
		
		for (int i = 0; i < this.roads.length; i++) {
			sum += this.roads[i].getLongestQueue();
		}
		
		return ((double) sum) / this.roads.length;
	}
	
	public void init(boolean red) {
		try {
			//Initiate roads
			for (int i = 0; i < this.roads.length; i++) {
				Road road = this.roads[i];
				
				road.carRemover = new CarRemover(road, this);
				
				try {
					road.carGenerator.activate();
				} catch (NullPointerException ex) {
					//Car generator is null
				}
			}
			
			if (red) {
				this.goGreen();
			} else {
				this.goRed();
			}
		} catch (SimulationException e) {
			e.printStackTrace();
		} catch (RestartException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isRed() {
		return this.red;
	}
	
	public void toggleLight() throws SimulationException, RestartException {
		if (this.red) {
			this.goGreen();
		} else {
			this.goRed();
		}
	}
	
	public String toString() {
		String string = "";
		
		for (int i = 0; i < this.roads.length; i++) {
			string += this.roads[i].toString();
		}
		
		return string;
	}
}
