package llm.stupidmulticrossing;

import java.util.Random;

import org.javasim.RestartException;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;

public class CarGenerator extends SimulationProcess {
	private float averageCarsPerHour;
	private Road road;
	
	public CarGenerator(Road road, float averageCarsPerHour) {
		this.setName("CarGenerator");
		this.averageCarsPerHour = averageCarsPerHour;
		this.road = road;
	}
	
	@Override
	public void run() {
		Random random = new Random();
		int millisecondsPerCar = (int)(1 / (this.averageCarsPerHour / (3600 * 1000)));
		long ms;
		
		//Generate car to road
		while (!this.terminated()) {
			try {
				ms = (long)((random.nextFloat() * (millisecondsPerCar * 2)));
				this.hold(ms);
				this.road.addCar();
			} catch (SimulationException e) {
				e.printStackTrace();
			} catch (RestartException e) {
				e.printStackTrace();
			}
		}
	}
}
