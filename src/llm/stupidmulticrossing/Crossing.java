package llm.stupidmulticrossing;

import org.javasim.RestartException;
import org.javasim.SimulationException;
import org.javasim.SimulationProcess;

public class Crossing extends SimulationProcess {
	RoadSynchronizer[] roadSynchronizers;
	private int lightTime;
	
	public Crossing(RoadSynchronizer[] roadSynchronizers, int lightTime) {
		this.setName("Crossing");
		this.roadSynchronizers = roadSynchronizers;
		this.lightTime = lightTime;
	}
	
	private void toggleLights() throws SimulationException, RestartException {
		for (int i = 0; i < this.roadSynchronizers.length; i++) {
			this.roadSynchronizers[i].toggleLight();
		}
		this.hold(this.lightTime);
	}
	
	public int getThroughput() {
		int sum = 0;
		
		for (int i = 0; i < this.roadSynchronizers.length; i++) {
			sum += this.roadSynchronizers[i].getThroughput();
		}
		return sum;
	}
	
	public RoadSynchronizer getPrimaryRoadSynchronizer() {
		return this.roadSynchronizers[0];
	}
	
	public RoadSynchronizer getSecondaryRoadSynchronizer() {
		return this.roadSynchronizers[1];
	}
	
	public void run() {
		//Initiate road synchronizers
		for (int i = 0; i < this.roadSynchronizers.length; i++) {
			this.roadSynchronizers[i].init((i == 0));
		}
		
		while (!this.terminated()) {
			try {
				this.toggleLights();
			} catch (SimulationException e) {
				e.printStackTrace();
			} catch (RestartException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String toString() {
		String string = "";
		
		for (int i = 0; i < this.roadSynchronizers.length; i++) {
			string += this.roadSynchronizers[i].toString();
		}
		
		return string;
	}
}